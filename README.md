#Advent of Code

Solutions to the Advent of Code initiative. You can find them [here]

## Sumary

- [Day 1](#day-1)
  - [Problem 1: Calculate sum](#problem-1-calculate-sum)
  - [Problem 2: Find dupes](#problem-2-find-dupes)
- [Day 2](#day-2)
  - [Problem 1: Checksum boxes](#problem-1-checksum-boxes)
  - [Problem 2: Missing box](#problem-2-missing-box)
- [Day 3](#day-3)
  - [Problem 1: Fabric](#problem-1-fabric)

## Day 1

### Problem 1: Calculate sum

First we load the list of elements into an array. We can do so in several ways.
I've choosen to do so by splitting it like so. First we split the text in lines
and then we parse each line into a number.

```javascript
var increments = `+12
-2
+3
-12
...
+534`.split('\n').map(el => +el);
```

To calculate the sum we can use `reduce(...)`. The following code will calculate
the sum of all the numbers in the array.

```javascript
increments.reduce((acum, curr) => acum + curr);
```

### Problem 2: Find dupes

To find the frequencies repeated first we load the numbers like we did in the
first problem this day. Then we iterate in a loop over the `increments` array.
Each time we calculate a new frequency we check against the already calculated
frequencies if there's some duplicate already.

```javascript
function dupes(increments){
  var freqs = [0];
  var curr;
  while(true){
      var last = freqs[freqs.length - 1];
      var curr = last + increments[(freqs.length - 1) % increments.length];
      if (freqs.includes(curr)) return 'solution: ' + curr + '!!!!';
      else freqs.push(curr);
  }
};
```

## Day 2

### Problem 1: Checksum boxes

To load the boxes, similarly to how we did in the **Day 1 - Problem 1** we split
the text in lines, no need to parse them. Having this done we can check the main
problem. First we analyze each "word" individually, checkig whether it contains
repeated letters. Then we get the ammount of them having two and having three.

```javascript
function checksum(boxes){
  var repeats = word => word
    .split('')
    .map((v,i,a) => a.filter(el => el === v).length)
    .filter((v,i,a) => a.indexOf(v) === i)
    .filter(v => [2,3].includes(v));
  var resumed = boxes.map(repeats)
  var twos = resumed.filter(el => el.includes(2)).length;
  var threes = resumed.filter(el => el.includes(3)).length;
  return twos * threes;
}
```

### Problem 2: Missing box

As we have to make an _all with all_ comparisson is more difficult here to use
array methods; but we will try anyway. First we only have to compare half of the
matrix, so we can use the first loop index as a pivot. Then, in the main part
it is only matter of checking the mask length. The mask contains the matching
letters in order. If the mask only lacks a letter then those are the pair of
boxes we are looking for.

```javascript
function findMissing(boxes) {
  const wordlen = boxes[0].length;
  for (let b = 0; b < boxes.length; b++) {
    const base = boxes[b];
    for (let p = b + 1; p < boxes.length; p++) {
      const pivot = boxes[p];
      const mask = base.filter((v,i)=>v === pivot[i]);
      const wrong = wordlen - mask.length;
      if (wrong === 1) return mask.join('');
    }
  }
}
```

## Day 3

### Problem 1: Fabric

We can use `processOrders()` to process an array of orders. First we create the
matrix that is going to hold the data. Then we parse each order, extracting the
left, top, width and height params with a regexp. Having those we can apply them
to the fabric matrix. Finally we `reduce` the fabric, counting only the squares
selected twice or more times.

```javascript
const fabricWidth = 1000;
const fabricHeight = 1000;
const fabric = Array(fabricWidth).fill(0).map(() => Array(fabricWidth).fill(0));

function mark(fabric, left, top, width, height) {
  for (let r = top; r < top + height; r++) {
    for (let c = left; c < left + width; c++) {
      fabric[r][c]++;
    }
  }
  return fabric
}

const processOrders = orders => orders
    .map(order => order.match(/^#\d+ @ (\d+),(\d+): (\d+)x(\d+)$/))
    .reduce((fabric, params) => {
      const [, left, top, width, height] = params;
      return mark(fabric, +left, +top, +width, +height);
    }, fabric)
    .reduce((sum, row) => sum + row.filter(selected => selected > 1).length, 0);
```


[here]: https://adventofcode.com/